var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var babelify = require("babelify");

gulp.task('nodemon', function() {
  nodemon({script: './server.js', exec: 'babel-node', ignore: ['assets/**']})
});

gulp.task('sass', function(){
  gulp.src('./assets/css/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./public/css/'));
});

gulp.task('babel', function(){
  var handleError = function (error){
    console.log('Error--', error);
  };

  var browserifySteam = browserify("./assets/js/app.js")
    .transform(babelify)
    .bundle();

  browserifySteam
    .pipe(source('app.js'))
    .pipe(gulp.dest('./public/js/'))
    .on('error', handleError);
});

gulp.task('watch', function() {
  gulp.watch('./assets/css/**/*.scss',['sass']);
});


gulp.task('compile', ['sass', 'babel']);

gulp.task('default', ['compile', 'nodemon']);

