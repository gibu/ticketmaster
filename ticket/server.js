const express = require('express');
var bodyParser = require('body-parser');
const app = express();
const FeedController = require('./app/controllers/feedController.js');

app.set('view engine', 'jade');
app.set('views', './views');
app.use(express.static('public'));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.render('index', {});
});

app.get('/v1/feeds', FeedController.fetchFeeds);
app.post('/v1/feeds', FeedController.createFeed);


var server = app.listen(3000, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log(`Default Server ${host}  ${port}`);
});
