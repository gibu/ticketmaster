var xml2js = require('xml2js');

var feed = '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
xmlns:georss="http://www.georss.org/georss" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:media="http://search.yahoo.com/mrss/"
  >

  <channel>
  <title>Re/code</title>
  <atom:link href="http://recode.net/feed/" rel="self" type="application/rss+xml" />
  <link>http://recode.net</link>
<description></description>
<lastBuildDate>Sat, 05 Sep 2015 12:45:27 +0000</lastBuildDate>
<language>en</language>
<sy:updatePeriod>hourly</sy:updatePeriod>
<sy:updateFrequency>1</sy:updateFrequency>
<generator>http://wordpress.com/</generator>
<cloud domain='recode.net' port='80' path='/?rsscloud=notify' registerProcedure='' protocol='http-post' />
  <image>
  <url>http://1.gravatar.com/blavatar/ffb3d8712b6311fcec3570217b252e8d?s=96&#038;d=http%3A%2F%2Fs2.wp.com%2Fi%2Fbuttonw-com.png</url>
<title>Re/code</title>
<link>http://recode.net</link>
</image>
<atom:link rel="search" type="application/opensearchdescription+xml" href="http://recode.net/osd.xml" title="Re/code" />
  <atom:link rel='hub' href='http://recode.net/?pushpress=hub'/>
  <item>
  <title>Amazon Hiring Staff for New Restaurant Division in Seattle, New York</title>
<link>http://recode.net/2015/09/04/amazon-hiring-staff-for-new-restaurant-division-in-seattle-new-york/</link>
<comments>http://recode.net/2015/09/04/amazon-hiring-staff-for-new-restaurant-division-in-seattle-new-york/#comments</comments>
<pubDate>Fri, 04 Sep 2015 23:19:40 +0000</pubDate>
<dc:creator><![CDATA[Reuters]]></dc:creator>
<category><![CDATA[Commerce]]></category>
<category><![CDATA[delivery]]></category>
<category><![CDATA[meal delivery]]></category>
<category><![CDATA[restaurants]]></category>

<guid isPermaLink="false">http://recode.net/?p=203514</guid>
<description><![CDATA[Job listings may signal the company's entry into the increasingly crowded meal delivery market.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203514&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/04/amazon-hiring-staff-for-new-restaurant-division-in-seattle-new-york/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/03/amazon-logo.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/03/amazon-logo.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">amazon-logo</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/6cf96847a1d7d14e4ed4a1104d9c0984?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">jamurrell</media:title>
</media:content>
</item>
<item>
<title>New York Ad Agency to Apple: Find Another Name for the iPhone 6s</title>
<link>http://recode.net/2015/09/04/new-york-ad-agency-to-apple-find-another-name-for-the-iphone-6s/</link>
<comments>http://recode.net/2015/09/04/new-york-ad-agency-to-apple-find-another-name-for-the-iphone-6s/#comments</comments>
<pubDate>Fri, 04 Sep 2015 21:03:32 +0000</pubDate>
<dc:creator><![CDATA[Dawn Chmielewski]]></dc:creator>
<category><![CDATA[Media]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[iPhone 6S]]></category>
<category><![CDATA[name change]]></category>

<guid isPermaLink="false">http://recode.net/?p=203411</guid>
<description><![CDATA[Ad agency 6S Marketing says it has been using the name 6S for the last 15 years.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203411&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/04/new-york-ad-agency-to-apple-find-another-name-for-the-iphone-6s/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/weare6s-times-square-billboard-sept-2015-v1-e1441393896584.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/weare6s-times-square-billboard-sept-2015-v1-e1441393896584.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">weare6s-times-square-billboard-sept-2015-v1</media:title>
</media:content>

<media:content url="http://2.gravatar.com/avatar/5fcf49b7d33e1aa85a28f2299d0fd96c?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">dawnchmielewski</media:title>
</media:content>
</item>
<item>
<title>A Sneak Peek at Some Apple-Produced TV Shows (Comic)</title>
<link>http://recode.net/2015/09/04/a-sneak-peek-at-some-apple-produced-tv-shows-comic/</link>
<comments>http://recode.net/2015/09/04/a-sneak-peek-at-some-apple-produced-tv-shows-comic/#comments</comments>
<pubDate>Fri, 04 Sep 2015 19:15:38 +0000</pubDate>
<dc:creator><![CDATA[Nitrozac & Snaggy]]></dc:creator>
<category><![CDATA[Media]]></category>
<category><![CDATA[Voices]]></category>
<category><![CDATA[Original content]]></category>
<category><![CDATA[TV shows]]></category>

<guid isPermaLink="false">http://recode.net/?p=203414</guid>
<description><![CDATA[Here is the latest comic from our Joy of Tech friends at <a href="http://www.geekculture.com/joyoftech/">Geek Culture</a>, Nitrozac &#38; Snaggy.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203414&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/04/a-sneak-peek-at-some-apple-produced-tv-shows-comic/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/unnamed-11.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/unnamed-11.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">JoT tv shows</media:title>
</media:content>

<media:content url="http://1.gravatar.com/avatar/7652000990d45c6b761ffdb0315de5d4?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">joebsf</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/unnamed1.jpg?quality=60&#038;strip=info" medium="image">
  <media:title type="html">Joy of Tech tv shows</media:title>
</media:content>
</item>
<item>
<title>Uber to Unveil Big E-Commerce Delivery Program With Retailers in the Fall</title>
<link>http://recode.net/2015/09/04/uber-to-unveil-big-e-commerce-delivery-program-with-retailers-in-the-fall/</link>
<comments>http://recode.net/2015/09/04/uber-to-unveil-big-e-commerce-delivery-program-with-retailers-in-the-fall/#comments</comments>
<pubDate>Fri, 04 Sep 2015 17:52:13 +0000</pubDate>
<dc:creator><![CDATA[Jason Del Rey]]></dc:creator>
<category><![CDATA[Commerce]]></category>
<category><![CDATA[General]]></category>
<category><![CDATA[same-day delivery]]></category>
<category><![CDATA[UberEverything]]></category>

<guid isPermaLink="false">http://recode.net/?p=203201</guid>
<description><![CDATA[If Uber wants to live up to its hype as the logistics network of the future, it will need to turn its delivery businesses into real businesses.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203201&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/04/uber-to-unveil-big-e-commerce-delivery-program-with-retailers-in-the-fall/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/05/uberfresh_630pxhedimg.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/05/uberfresh_630pxhedimg.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">uberfresh_630pxhedimg</media:title>
</media:content>

<media:content url="http://1.gravatar.com/avatar/71dc3afc9370c1555ca7775fc4bc0144?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">jasondelrey</media:title>
</media:content>
</item>
<item>
<title>Toyota to Invest $50 Million in New Artificial Intelligence, Partner With Stanford and MIT</title>
<link>http://recode.net/2015/09/04/toyota-to-invest-50-million-in-new-artificial-intelligence-partner-with-stanford-and-mit/</link>
<comments>http://recode.net/2015/09/04/toyota-to-invest-50-million-in-new-artificial-intelligence-partner-with-stanford-and-mit/#comments</comments>
<pubDate>Fri, 04 Sep 2015 17:51:16 +0000</pubDate>
<dc:creator><![CDATA[Ina Fried]]></dc:creator>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[automotive]]></category>
<category><![CDATA[autonomous driving]]></category>
<category><![CDATA[cars]]></category>

<guid isPermaLink="false">http://recode.net/?p=203386</guid>
<description><![CDATA[The company has also poached top DARPA official Gill Pratt to lead its efforts.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203386&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/04/toyota-to-invest-50-million-in-new-artificial-intelligence-partner-with-stanford-and-mit/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/toyota-autonomous.png?w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/toyota-autonomous.png" medium="image">
  <media:title type="html">20150904 toyota-autonomous</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/cdeb4c000e1f7ceed0b53c491badfd81?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">inafried</media:title>
</media:content>
</item>
<item>
<title>BlackBerry Scooping Up Rival Device Management Company Good Technology for $425 Million in Cash</title>
<link>http://recode.net/2015/09/04/blackberry-scooping-up-rival-device-management-company-good-technology-for-450-million-in-cash/</link>
<comments>http://recode.net/2015/09/04/blackberry-scooping-up-rival-device-management-company-good-technology-for-450-million-in-cash/#comments</comments>
<pubDate>Fri, 04 Sep 2015 14:59:09 +0000</pubDate>
<dc:creator><![CDATA[Ina Fried]]></dc:creator>
<category><![CDATA[Enterprise]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[Android]]></category>
<category><![CDATA[BlackBerry]]></category>
<category><![CDATA[enterprise]]></category>
<category><![CDATA[iOS]]></category>
<category><![CDATA[mobile device management]]></category>

<guid isPermaLink="false">http://recode.net/?p=203346</guid>
<description><![CDATA[With the deal, BlackBerry is doubling down on its core business of helping large companies manage their workers' phones and tablets.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203346&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/04/blackberry-scooping-up-rival-device-management-company-good-technology-for-450-million-in-cash/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2014/05/code-20140528-143619-5240-e1435072652789.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2014/05/code-20140528-143619-5240-e1435072652789.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">John Chen, Blackberry, Code Conference</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/cdeb4c000e1f7ceed0b53c491badfd81?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">inafried</media:title>
</media:content>
</item>
<item>
<title>Please Don&#8217;t Ask Google &#8216;Who Runs Hollywood&#8217;</title>
<link>http://recode.net/2015/09/04/please-dont-ask-google-who-runs-hollywood/</link>
<comments>http://recode.net/2015/09/04/please-dont-ask-google-who-runs-hollywood/#comments</comments>
<pubDate>Fri, 04 Sep 2015 13:27:06 +0000</pubDate>
<dc:creator><![CDATA[Mark Bergen]]></dc:creator>
<category><![CDATA[Media]]></category>
<category><![CDATA[Anti-Defamation League]]></category>
<category><![CDATA[Anti-Semitism]]></category>
<category><![CDATA[direct answers]]></category>
<category><![CDATA[Google Search]]></category>
<category><![CDATA[search]]></category>

<guid isPermaLink="false">http://recode.net/?p=203249</guid>
<description><![CDATA[Why the robotic "direct answers" in search can surface the underbelly of the Web.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203249&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/04/please-dont-ask-google-who-runs-hollywood/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/thinkstockphotos-494096447-e1441398926303.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/thinkstockphotos-494096447-e1441398926303.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">2015090415-holloywood-and-vine-street-sign</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/9f4829ca5d76a2f4b82673487dc76a69?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">mhbergen</media:title>
</media:content>
</item>
<item>
<title>Amazon Bulks Up on Video Tech</title>
<link>http://recode.net/2015/09/04/amazon-bulks-up-on-video-tech/</link>
<comments>http://recode.net/2015/09/04/amazon-bulks-up-on-video-tech/#comments</comments>
<pubDate>Fri, 04 Sep 2015 12:03:05 +0000</pubDate>
<dc:creator><![CDATA[Noah Kulwin]]></dc:creator>
<category><![CDATA[Re/code Daily]]></category>

<guid isPermaLink="false">http://recode.net/2015/09/04/amazon-bulks-up-on-video-tech/</guid>
<description><![CDATA[2015's latest major video-related acquisition.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203311&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/04/amazon-bulks-up-on-video-tech/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2014/06/shutterstock_173368031_edited.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2014/06/shutterstock_173368031_edited.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">video tablet</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/02b8d91f1c27f75686829b5082598352?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">nkulwin</media:title>
</media:content>
</item>
<item>
<title>Director Alex Gibney Says &#8216;Steve Jobs: The Man in the Machine&#8217; Is a &#8216;Corrective&#8217; (Q&#038;A)</title>
<link>http://recode.net/2015/09/04/director-alex-gibney-says-steve-jobs-the-man-in-the-machine-is-a-corrective/</link>
<comments>http://recode.net/2015/09/04/director-alex-gibney-says-steve-jobs-the-man-in-the-machine-is-a-corrective/#comments</comments>
<pubDate>Fri, 04 Sep 2015 11:00:51 +0000</pubDate>
<dc:creator><![CDATA[Peter Kafka]]></dc:creator>
<category><![CDATA[Culture]]></category>
<category><![CDATA[Media]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[Movies]]></category>
<category><![CDATA[South By Southwest]]></category>
<category><![CDATA[Steve Jobs: The Man in the Machine]]></category>
<category><![CDATA[SXSW]]></category>

<guid isPermaLink="false">http://recode.net/?p=203200</guid>
<description><![CDATA["There’s been a lot of hagiography of Steve. I don’t think that does justice to the man."<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203200&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/04/director-alex-gibney-says-steve-jobs-the-man-in-the-machine-is-a-corrective/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/steve-jobs-office.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/steve-jobs-office.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">Steve Jobs from Steve Jobs: The Man in the Machine</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/f1ef7a480bbe6af8e9409b35ac62e539?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">pakafka</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/steve-jobs-socks.jpg?quality=60&#038;strip=info" medium="image">
  <media:title type="html">Steve Jobs from Steve Jobs: The Man in the Machine</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/steve-jobs-getty.jpg?quality=60&#038;strip=info" medium="image">
  <media:title type="html">Steve Jobs</media:title>
</media:content>
</item>
<item>
<title>The Real Software Revolution? It&#8217;s in the Data Center.</title>
<link>http://recode.net/2015/09/03/the-real-software-revolution-its-in-the-data-center/</link>
<comments>http://recode.net/2015/09/03/the-real-software-revolution-its-in-the-data-center/#comments</comments>
<pubDate>Thu, 03 Sep 2015 21:36:52 +0000</pubDate>
<dc:creator><![CDATA[Bob O'Donnell]]></dc:creator>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[Voices]]></category>
<category><![CDATA[data center]]></category>
<category><![CDATA[desktop virtualization]]></category>
<category><![CDATA[Grid 2.0]]></category>
<category><![CDATA[hardware abstraction]]></category>
<category><![CDATA[hyperconvergence]]></category>
<category><![CDATA[storage area networks]]></category>
<category><![CDATA[Virtual Desktop Infrastructure]]></category>
<category><![CDATA[virtualization]]></category>
<category><![CDATA[VMworld]]></category>

<guid isPermaLink="false">http://recode.net/?p=203119</guid>
<description><![CDATA["Desktop virtualization" and "hyperconvergence" are playing increasingly essential roles for the devices and services that keep us engaged every day.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203119&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/the-real-software-revolution-its-in-the-data-center/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/convergence.png?w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/convergence.png" medium="image">
  <media:title type="html">convergence</media:title>
</media:content>

<media:content url="http://1.gravatar.com/avatar/7652000990d45c6b761ffdb0315de5d4?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">joebsf</media:title>
</media:content>
</item>
<item>
<title>Qualcomm Exploring Sale of Vuforia Augmented Reality Unit</title>
<link>http://recode.net/2015/09/03/qualcomm-exploring-sale-of-vuforia-augmented-reality-unit/</link>
<comments>http://recode.net/2015/09/03/qualcomm-exploring-sale-of-vuforia-augmented-reality-unit/#comments</comments>
<pubDate>Thu, 03 Sep 2015 20:49:40 +0000</pubDate>
<dc:creator><![CDATA[Ina Fried]]></dc:creator>
<category><![CDATA[General]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[Augmented reality]]></category>
<category><![CDATA[Virtual reality]]></category>

<guid isPermaLink="false">http://recode.net/?p=203120</guid>
<description><![CDATA[The move comes as the San Diego-based company looks to cut $1.4 billion in costs and exit non-core businesses.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203120&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/qualcomm-exploring-sale-of-vuforia-augmented-reality-unit/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/04/steve-mollekopf-qualcomm.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/04/steve-mollekopf-qualcomm.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">steve-mollekopf-qualcomm</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/cdeb4c000e1f7ceed0b53c491badfd81?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">inafried</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/vuforia-qualcomm.jpg?quality=60&#038;strip=info" medium="image">
  <media:title type="html">20150903 vuforia-qualcomm</media:title>
</media:content>
</item>
<item>
<title>Google VP Joins Former Business Chief Arora at SoftBank</title>
<link>http://recode.net/2015/09/03/google-vp-joins-former-business-chief-arora-at-softbank/</link>
<comments>http://recode.net/2015/09/03/google-vp-joins-former-business-chief-arora-at-softbank/#comments</comments>
<pubDate>Thu, 03 Sep 2015 19:51:28 +0000</pubDate>
<dc:creator><![CDATA[Mark Bergen]]></dc:creator>
<category><![CDATA[Media]]></category>
<category><![CDATA[Comings and Goings]]></category>
<category><![CDATA[human resources]]></category>
<category><![CDATA[venture capital]]></category>

<guid isPermaLink="false">http://recode.net/?p=203087</guid>
<description><![CDATA[Google's old sales boss is getting the gang back together at SoftBank.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203087&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/03/google-vp-joins-former-business-chief-arora-at-softbank/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2014/01/nikesh_arora.png?w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2014/01/nikesh_arora.png" medium="image">
  <media:title type="html">nikesh_arora</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/9f4829ca5d76a2f4b82673487dc76a69?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">mhbergen</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/liane-hornsey.jpg?quality=60&#038;strip=info" medium="image">
  <media:title type="html">Liane Hornsey, director of public affairs, SoftBank</media:title>
</media:content>
</item>
<item>
<title>The Rebecca Minkoff Virtual Reality Headset Is Here</title>
<link>http://recode.net/2015/09/03/the-rebecca-minkoff-virtual-reality-headset-is-here/</link>
<comments>http://recode.net/2015/09/03/the-rebecca-minkoff-virtual-reality-headset-is-here/#comments</comments>
<pubDate>Thu, 03 Sep 2015 19:40:20 +0000</pubDate>
<dc:creator><![CDATA[Erika Adams, Racked and Adele Chapin, Racked]]></dc:creator>
<category><![CDATA[Culture]]></category>
<category><![CDATA[Gaming]]></category>
<category><![CDATA[General]]></category>
<category><![CDATA[fashion]]></category>
<category><![CDATA[headset]]></category>
<category><![CDATA[Virtual reality]]></category>
<category><![CDATA[VR]]></category>

<guid isPermaLink="false">http://recode.net/?p=203092</guid>
<description><![CDATA[The fashion brand has filmed a runway show specifically for people to watch in a cardboard headset. <img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203092&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/the-rebecca-minkoff-virtual-reality-headset-is-here/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/rebeca-minkoff-e1441309161406.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/rebeca-minkoff-e1441309161406.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">rebeca-minkoff</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/9940a9ccbb19554f7b093fd9ee58f6f8?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">madfoot</media:title>
</media:content>
</item>
<item>
<title>Samsung Galaxy Tab S2 Review</title>
<link>http://recode.net/2015/09/03/samsung-galaxy-tab-s2-review/</link>
<comments>http://recode.net/2015/09/03/samsung-galaxy-tab-s2-review/#comments</comments>
<pubDate>Thu, 03 Sep 2015 19:38:16 +0000</pubDate>
<dc:creator><![CDATA[Lauren Goode]]></dc:creator>
<category><![CDATA[Product News]]></category>
<category><![CDATA[Reviews]]></category>
<category><![CDATA[Galaxy Tab S2]]></category>
<category><![CDATA[tablets]]></category>

<guid isPermaLink="false">http://recode.net/?p=203091</guid>
<description><![CDATA[Last year's tech in this year's tablet.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203091&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/samsung-galaxy-tab-s2-review/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/20150903-samsung-galaxy-tab-s2.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/20150903-samsung-galaxy-tab-s2.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">20150903-samsung-galaxy-tab-s2</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/630cc471a9cad5e872ea570bfa72a32c?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">laurengoode</media:title>
</media:content>
</item>
<item>
<title>Google Dreams of a World After Apps but It&#8217;s a Nightmare for Rivals</title>
                                                                          <link>http://recode.net/2015/09/03/google-dreams-of-a-world-after-apps-but-its-a-nightmare-for-rivals/</link>
<comments>http://recode.net/2015/09/03/google-dreams-of-a-world-after-apps-but-its-a-nightmare-for-rivals/#comments</comments>
<pubDate>Thu, 03 Sep 2015 18:37:44 +0000</pubDate>
<dc:creator><![CDATA[Mark Bergen]]></dc:creator>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[app ads]]></category>
<category><![CDATA[app indexing]]></category>
<category><![CDATA[app install ads]]></category>
<category><![CDATA[apps]]></category>
<category><![CDATA[deep link]]></category>
<category><![CDATA[deep linking]]></category>
<category><![CDATA[Google Now]]></category>
<category><![CDATA[mobile app install ads]]></category>
<category><![CDATA[Now on Tap]]></category>

<guid isPermaLink="false">http://recode.net/?p=203007</guid>
<description><![CDATA["In an app world, Google is nothing."<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203007&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/google-dreams-of-a-world-after-apps-but-its-a-nightmare-for-rivals/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/shutterstock_293063258-e1441301996927.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/shutterstock_293063258-e1441301996927.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">shutterstock_293063258</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/9f4829ca5d76a2f4b82673487dc76a69?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">mhbergen</media:title>
</media:content>
</item>
<item>
<title>Get Ready for Apple TV Games</title>
<link>http://recode.net/2015/09/03/get-ready-for-apple-tv-games/</link>
<comments>http://recode.net/2015/09/03/get-ready-for-apple-tv-games/#comments</comments>
<pubDate>Thu, 03 Sep 2015 18:21:10 +0000</pubDate>
<dc:creator><![CDATA[Dawn Chmielewski]]></dc:creator>
<category><![CDATA[Gaming]]></category>
<category><![CDATA[Media]]></category>
<category><![CDATA[Apple iOS App Store]]></category>
<category><![CDATA[Apple TV]]></category>
<category><![CDATA[Games]]></category>

<guid isPermaLink="false">http://recode.net/?p=202659</guid>
<description><![CDATA[The next Apple TV will likely boast a speedier processor and new remote control that would make it great for gaming.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=202659&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/get-ready-for-apple-tv-games/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2014/11/gifts-for-boomer-apple-tv.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2014/11/gifts-for-boomer-apple-tv.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">Apple TV</media:title>
</media:content>

<media:content url="http://2.gravatar.com/avatar/5fcf49b7d33e1aa85a28f2299d0fd96c?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">dawnchmielewski</media:title>
</media:content>
</item>
<item>
<title>Walkman to iPod: Business-Model Transformation</title>
<link>http://recode.net/2015/09/03/book-excerpt-business-model-transformation/</link>
<comments>http://recode.net/2015/09/03/book-excerpt-business-model-transformation/#comments</comments>
<pubDate>Thu, 03 Sep 2015 18:00:00 +0000</pubDate>
<dc:creator><![CDATA[R. "Ray" Wang]]></dc:creator>
<category><![CDATA[Commerce]]></category>
<category><![CDATA[Enterprise]]></category>
<category><![CDATA[Media]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[Voices]]></category>
<category><![CDATA[BlackBerry]]></category>
<category><![CDATA[business model]]></category>
<category><![CDATA[iPhone]]></category>
<category><![CDATA[iPod]]></category>
<category><![CDATA[Walkman]]></category>

<guid isPermaLink="false">http://recode.net/?p=149120</guid>
<description><![CDATA[Samsung wants to be Apple. And it could be.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=149120&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/book-excerpt-business-model-transformation/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/fathertnjlq.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/fathertnjlq.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">walkman vs. iPod</media:title>
</media:content>

<media:content url="http://1.gravatar.com/avatar/7652000990d45c6b761ffdb0315de5d4?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">joebsf</media:title>
</media:content>
</item>
<item>
<title>Sony, Former Workers Settle Suit Over Hacking</title>
<link>http://recode.net/2015/09/03/sony-former-workers-settle-suit-over-hacking/</link>
<comments>http://recode.net/2015/09/03/sony-former-workers-settle-suit-over-hacking/#comments</comments>
<pubDate>Thu, 03 Sep 2015 17:52:15 +0000</pubDate>
<dc:creator><![CDATA[Reuters]]></dc:creator>
<category><![CDATA[Security]]></category>
<category><![CDATA[hacking]]></category>
<category><![CDATA[lawsuits]]></category>
<category><![CDATA[litigation]]></category>
<category><![CDATA[Sony hack]]></category>

<guid isPermaLink="false">http://recode.net/?p=203058</guid>
<description><![CDATA[The workers said Sony's negligence forced them to beef up credit monitoring to address their greater risk of identity theft.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203058&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/03/sony-former-workers-settle-suit-over-hacking/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/20150903-sony-hack-interview.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/20150903-sony-hack-interview.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">20150903-sony-hack-interview</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/6cf96847a1d7d14e4ed4a1104d9c0984?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">jamurrell</media:title>
</media:content>
</item>
<item>
<title>SmartThings’ New Hub Uses Samsung Cameras to Monitor Your Home</title>
<link>http://recode.net/2015/09/03/smartthings-new-hub-uses-samsung-cameras-to-monitor-your-home/</link>
<comments>http://recode.net/2015/09/03/smartthings-new-hub-uses-samsung-cameras-to-monitor-your-home/#comments</comments>
<pubDate>Thu, 03 Sep 2015 17:06:27 +0000</pubDate>
<dc:creator><![CDATA[Lauren Goode]]></dc:creator>
<category><![CDATA[Product News]]></category>
<category><![CDATA[connected home]]></category>
<category><![CDATA[smart home]]></category>

<guid isPermaLink="false">http://recode.net/?p=203025</guid>
<description><![CDATA["Before, SmartThings was easy for early adopters, but I think now we're mass market easy."<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203025&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/smartthings-new-hub-uses-samsung-cameras-to-monitor-your-home/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/20150903-smarthings-smart-home.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/20150903-smarthings-smart-home.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">20150903-smarthings-smart-home</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/630cc471a9cad5e872ea570bfa72a32c?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">laurengoode</media:title>
</media:content>
</item>
<item>
<title>Facebook Is Testing Its Fancy New Mobile Ads</title>
<link>http://recode.net/2015/09/03/facebook-is-testing-its-fancy-new-mobile-ads/</link>
<comments>http://recode.net/2015/09/03/facebook-is-testing-its-fancy-new-mobile-ads/#comments</comments>
<pubDate>Thu, 03 Sep 2015 17:00:34 +0000</pubDate>
<dc:creator><![CDATA[Kurt Wagner]]></dc:creator>
<category><![CDATA[Media]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[Social]]></category>
<category><![CDATA[hosted content]]></category>
<category><![CDATA[mobile ads]]></category>

<guid isPermaLink="false">http://recode.net/?p=202711</guid>
<description><![CDATA[Facebook first showed off the new ad at Cannes Lions back in June. <img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=202711&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/facebook-is-testing-its-fancy-new-mobile-ads/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/fb-ads-new.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/fb-ads-new.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">fb-ads-new</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/f9db238d36e1350a735dcd76018de4d7?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">kurtwag88</media:title>
</media:content>
</item>
<item>
<title>Here&#8217;s Daniel Radcliffe as One of the Creators of Grand Theft Auto (Video)</title>
<link>http://recode.net/2015/09/03/heres-daniel-radcliffe-as-one-of-the-creators-of-grand-theft-auto-video/</link>
<comments>http://recode.net/2015/09/03/heres-daniel-radcliffe-as-one-of-the-creators-of-grand-theft-auto-video/#comments</comments>
<pubDate>Thu, 03 Sep 2015 16:38:02 +0000</pubDate>
<dc:creator><![CDATA[Eric Johnson]]></dc:creator>
<category><![CDATA[Gaming]]></category>
<category><![CDATA[General]]></category>
<category><![CDATA[Media]]></category>
<category><![CDATA[Gamechangers]]></category>
<category><![CDATA[Grand Theft Auto]]></category>
<category><![CDATA[GTA]]></category>
<category><![CDATA[movie]]></category>

<guid isPermaLink="false">http://recode.net/?p=203004</guid>
<description><![CDATA[Harry, who are these new friends and what happened to Ron and Hermione?<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=203004&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/heres-daniel-radcliffe-as-one-of-the-creators-of-grand-theft-auto-video/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/20150903-daniel-radcliffe-gta-gamechangers.png?w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/20150903-daniel-radcliffe-gta-gamechangers.png" medium="image" />

  <media:content url="http://0.gravatar.com/avatar/90aa501837d87b78fee71d06e122c6f5?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">heyheyesj</media:title>
</media:content>
</item>
<item>
<title>Cruising the Carpool: Are Lyft Line and UberPool the New Tinder?</title>
<link>http://recode.net/2015/09/03/cruising-the-carpool-are-lyft-line-and-uberpool-the-new-tinder/</link>
<comments>http://recode.net/2015/09/03/cruising-the-carpool-are-lyft-line-and-uberpool-the-new-tinder/#comments</comments>
<pubDate>Thu, 03 Sep 2015 16:00:35 +0000</pubDate>
<dc:creator><![CDATA[Carmel DeAmicis]]></dc:creator>
<category><![CDATA[Commerce]]></category>
<category><![CDATA[Culture]]></category>
<category><![CDATA[General]]></category>
<category><![CDATA[Mobile]]></category>
<category><![CDATA[dating]]></category>
<category><![CDATA[love]]></category>
<category><![CDATA[Lyft Line]]></category>
<category><![CDATA[romance]]></category>
<category><![CDATA[UberPool]]></category>

<guid isPermaLink="false">http://recode.net/?p=200378</guid>
<description><![CDATA[People are finding unexpected -- or premeditated -- romance in rideshare carpooling.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=200378&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/cruising-the-carpool-are-lyft-line-and-uberpool-the-new-tinder/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-14.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-14.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">20150902-20150902-lyft-line-uber-pool-dating-carmel-</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/c92b157c2f68939b9f5cb20feaee8ff6?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">carmelrecode</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/uberlyft.jpeg?quality=60&#038;strip=info&#038;w=640" medium="image" />

  <media:content url="https://recodetech.files.wordpress.com/2015/08/img_0757.jpg?quality=60&#038;strip=info&#038;w=480" medium="image">
  <media:title type="html">Shonna (left) and Sasha (right) in their Lyft Pick Up Line Halloween costume</media:title>
</media:content>
</item>
<item>
<title>The Night I Went Speed Dating on UberPool and Lyft Line</title>
<link>http://recode.net/2015/09/03/the-night-i-went-speed-dating-on-uberpool-and-lyft-line/</link>
<comments>http://recode.net/2015/09/03/the-night-i-went-speed-dating-on-uberpool-and-lyft-line/#comments</comments>
<pubDate>Thu, 03 Sep 2015 16:00:28 +0000</pubDate>
<dc:creator><![CDATA[Carmel DeAmicis]]></dc:creator>
<category><![CDATA[Culture]]></category>
<category><![CDATA[General]]></category>
<category><![CDATA[dating romance]]></category>
<category><![CDATA[love]]></category>
<category><![CDATA[Lyft Line]]></category>
<category><![CDATA[speed dating]]></category>
<category><![CDATA[UberPool]]></category>

<guid isPermaLink="false">http://recode.net/?p=201544</guid>
<description><![CDATA[To find someone you're attracted to who feels the same, you have to take a lot of rides. <img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=201544&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
<wfw:commentRss>http://recode.net/2015/09/03/the-night-i-went-speed-dating-on-uberpool-and-lyft-line/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-10.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-10.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">20150902-20150902-lyft-line-uber-pool-dating-carmel-</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/c92b157c2f68939b9f5cb20feaee8ff6?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">carmelrecode</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-9.jpg?quality=60&#038;strip=info&#038;w=640" medium="image">
  <media:title type="html">The author of this article talks to her Lyft driver.</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-3.jpg?quality=60&#038;strip=info&#038;w=640" medium="image">
  <media:title type="html">20150902-20150902-lyft-line-uber-pool-dating-carmel-</media:title>
</media:content>

<media:content url="https://recodetech.files.wordpress.com/2015/09/20150902-20150902-lyft-line-uber-pool-dating-carmel-5.jpg?quality=60&#038;strip=info&#038;w=640" medium="image">
  <media:title type="html">20150902-20150902-lyft-line-uber-pool-dating-carmel-</media:title>
</media:content>
</item>
<item>
<title>Dropbox Lands Arizona State as Enterprise Customer, Hires Education Unit Head</title>
<link>http://recode.net/2015/09/03/dropbox-lands-arizona-state-as-enterprise-customer-hires-education-unit-head/</link>
<comments>http://recode.net/2015/09/03/dropbox-lands-arizona-state-as-enterprise-customer-hires-education-unit-head/#comments</comments>
<pubDate>Thu, 03 Sep 2015 16:00:23 +0000</pubDate>
<dc:creator><![CDATA[Arik Hesseldahl]]></dc:creator>
<category><![CDATA[Enterprise]]></category>
<category><![CDATA[cloud collaboration]]></category>
<category><![CDATA[cloud services]]></category>
<category><![CDATA[cloud storage]]></category>
<category><![CDATA[Comings and Goings]]></category>

<guid isPermaLink="false">http://recode.net/?p=202929</guid>
<description><![CDATA[A new focus on higher education in the cloud.  <img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=202929&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/dropbox-lands-arizona-state-as-enterprise-customer-hires-education-unit-head/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/arizona_state_university.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/arizona_state_university.jpg?quality=80&#38;strip=info" medium="image" />

  <media:content url="http://0.gravatar.com/avatar/611990617c72d956811510998fc036a0?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">ahess247</media:title>
</media:content>
</item>
<item>
<title>Tastemade Graduates a Web Video Show to TV</title>
<link>http://recode.net/2015/09/03/tastemade-graduates-a-web-video-show-to-tv/</link>
<comments>http://recode.net/2015/09/03/tastemade-graduates-a-web-video-show-to-tv/#comments</comments>
<pubDate>Thu, 03 Sep 2015 15:40:07 +0000</pubDate>
<dc:creator><![CDATA[Peter Kafka]]></dc:creator>
<category><![CDATA[General]]></category>
<category><![CDATA[Media]]></category>

<guid isPermaLink="false">http://recode.net/?p=202934</guid>
<description><![CDATA["The Grill Iron," a series of shorts about college football and barbeque that Tastemade ran last year, will be packaged into 30-minute shows for the TV network.<img alt="" border="0" src="http://pixel.wp.com/b.gif?host=recode.net&#038;blog=61371898&#038;post=202934&#038;subd=recodetech&#038;ref=&#038;feed=1" width="1" height="1" />]]></description>
  <wfw:commentRss>http://recode.net/2015/09/03/tastemade-graduates-a-web-video-show-to-tv/feed/</wfw:commentRss>
<slash:comments>0</slash:comments>

<media:thumbnail url="http://recodetech.files.wordpress.com/2015/09/the-grill-iron1.jpg?quality=80&#038;strip=info&#038;w=200" />
  <media:content url="https://recodetech.files.wordpress.com/2015/09/the-grill-iron1.jpg?quality=80&#38;strip=info" medium="image">
  <media:title type="html">the-grill-iron1</media:title>
</media:content>

<media:content url="http://0.gravatar.com/avatar/f1ef7a480bbe6af8e9409b35ac62e539?s=96&#38;d=identicon&#38;r=G" medium="image">
  <media:title type="html">pakafka</media:title>
</media:content>
</item>
</channel>
</rss>'