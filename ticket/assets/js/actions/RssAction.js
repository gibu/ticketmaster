const ActionTypes = require('../constants/RssConstants.js').ACTION_TYPES;
const Dispatcher = require('../dispatcher/Dispatcher.js');
const RssWeb = require('../web_utils/RssWeb.js');

class RssAction {
  fetchFeeds(){
    console.log('Fetch feeds');
    Dispatcher.dispatch({
      actionType: ActionTypes.FETCH_FEEDS_IN_PROGRES
    });
    RssWeb.fetchFeeds();
  }

  showDetail(guid){
    Dispatcher.dispatch({
      actionType: ActionTypes.SHOW_FEED_DETAIL,
      guid: guid
    });
  }

  selectFeed(feedName){
    Dispatcher.dispatch({
      actionType: ActionTypes.SELECT_FEED,
      feedName: feedName
    });
  }

  changeFilter(filter){
    Dispatcher.dispatch({
      actionType: ActionTypes.CHANGE_FILTER,
      filter: filter
    });
  }

  toggleFavourite(guid){
    Dispatcher.dispatch({
      actionType: ActionTypes.TOGGLE_FAVOURITE,
      guid: guid
    })
  }

  changeQuery(query){
    Dispatcher.dispatch({
      actionType: ActionTypes.CHANGE_QUERY,
      query: query
    })
  }

  addFeed(url){
    Dispatcher.dispatch({
      actionType: ActionTypes.ADD_FEED_IN_PROGRESS
    });
    RssWeb.addFeed(url);
  }

  changeSort(sort){
    Dispatcher.dispatch({
      actionType: ActionTypes.CHANGE_SORT,
      sort: sort
    })
  }
};

module.exports = new RssAction();