const _ = require('lodash');
const React = require('react');
const R = React.DOM;
const MenuItem = React.createFactory(require('./components/MenuItem.js'));
const MenuButton = React.createFactory(require('./components/MenuButton.js'));

var Menu = React.createClass({
  propTypes: {
    items: React.PropTypes.array.isRequired
  },

  getInitialState: function(){
    return {
      expanded: false
    };
  },

  onClickSubscription: function(e) {
    e.preventDefault();
    this.setState({expanded: !this.state.expanded})
  },

  render: function() {
    var menuItems = _.map(this.props.items, (item) =>{
      return MenuItem(item);
    });

    var className = 'menuItems';
    if(this.state.expanded){
      className += ' expanded';
    };

    return R.nav({},
      MenuButton({onClick: this.onClickSubscription, expanded: this.state.expanded}),
      R.ul({className}, menuItems)
    );
  }
});

module.exports = Menu;