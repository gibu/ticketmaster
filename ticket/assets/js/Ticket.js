var React = require('react');
var R = React.DOM;
const Menu = React.createFactory(require('./Menu.js'));
const Reader = React.createFactory(require('./components/Reader.js'));
const RssStore = require('./stores/RssStore.js');
const RssAction = require('./actions/RssAction');


var _getDataFromStore = () => {
  return {
    feeds: RssStore.getFeeds(),
    menuItems: RssStore.getMenuItems(),
    filters: RssStore.getFilters()
  }
}

var Ticket = React.createClass({
  getInitialState: function() {
    return _getDataFromStore();
  },

  componentDidMount: function(){
    RssStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    RssStore.removeChangeListener(this.onChange);
  },

  onChange: function(){
    this.setState(_getDataFromStore());
  },

  render: function() {
    return R.div({id: "app"},
      Menu({items: this.state.menuItems}),
      R.section({id: "content"},
        Reader({feeds: this.state.feeds, filters: this.state.filters})
      )
    );
  }
});

module.exports = Ticket;
