var _ = require('lodash');
var React = require('react');
var R = React.DOM;
const RssAction = require('../actions/RssAction');

var Query = React.createClass({
  getInitialState: function(){
    return {
      query: null
    }
  },

  _onChange: function(e) {
    var query = e.target.value;
    this.setState({query});
    RssAction.changeQuery(query);
  },

  render: function() {
    return R.input({type: 'text', placeholder: 'Filter your feeds', value: this.state.query, onChange: this._onChange})
  }
});

module.exports = Query;