const _ = require('lodash');
const React = require('react');
const R = React.DOM;
const Feed = React.createFactory(require('./Feed.js'));

var Feeds = React.createClass({
  propTypes: {
    feeds: React.PropTypes.array.isRequired,
  },

  render: function() {
    var feeds = _.map(this.props.feeds, (feed) => Feed({feed}));
    return R.div({className: 'Feeds'},
      feeds
    );
  }
});

module.exports = Feeds;