var _ = require('lodash');
var React = require('react');
var R = React.DOM;
const RssAction = require('../actions/RssAction');

var AddFeed = React.createClass({
  getInitialState: function(){
    return {
      url: null
    }
  },

  _onChange: function(e) {
    var url = e.target.value;
    this.setState({url});
    //RssAction.changeQuery(query);
  },

  _onClickAddFeed: function(e){
    e.preventDefault();
    RssAction.addFeed(this.state.url);
  },

  render: function() {
    return R.span({className: 'AddFeed'},
      R.input({type: 'text', placeholder: 'Add new feed', value: this.state.url, onChange: this._onChange}),
      R.span({className: 'button', onClick: this._onClickAddFeed}, 'Add Feed')
    )
  }
});

module.exports = AddFeed;