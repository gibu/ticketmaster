var _ = require('lodash');
var React = require('react');
var R = React.DOM;
const RssAction = require('../actions/RssAction');

const SORTS = [
  {value: 'date_desc', name: 'Date Desc'},
  {value: 'date_asc', name: 'Date Asc'},
  {value: 'title_desc', name: 'Title Desc'},
  {value: 'title_asc', name: 'Title Asc'}
]

var Sort = React.createClass({
  propTypes: {
  },

  getInitialState: function() {
    return {
      open: false,
      selected: null
    }
  },

  _toogle: function(){
    this.setState({open: !this.state.open})
  },

  _changeSort: function(e){
    var selected = e.target.value;
    this.setState({selected});
    RssAction.changeSort(selected);
  },

  render: function() {
    var containerClass = 'SortContainer';
    if(this.state.open){
      containerClass += ' Active';
    }

    var self = this;
    var radios = SORTS.map((order) => {
      return R.span({},
        R.input({type: 'radio', value: order.value, checked: (order.value === self.state.selected), onChange: self._changeSort}, order.name)
      );
    });

    return R.span({className: 'Sort'},
      R.span({className: 'button',  onClick: this._toogle}, "Sort"),
      R.span({className: containerClass},
        radios
      )
    );
  }
});

module.exports = Sort;