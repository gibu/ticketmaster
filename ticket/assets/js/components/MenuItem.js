var _ = require('lodash');
var React = require('react');
var R = React.DOM;
const RssAction = require('../actions/RssAction');

var MenuItem = React.createClass({
  propTypes: {
    title: React.PropTypes.string.isRequired,
    selected: React.PropTypes.bool
  },

  _onClick: function () {
    RssAction.selectFeed(this.props.title)
  },

  render: function() {
    var className = '';
    if(this.props.selected){
      className = 'Selected';
    }
    return R.li({},
      R.a({className: className, onClick: this._onClick}, this.props.title)
    );
  }
});

module.exports = MenuItem;