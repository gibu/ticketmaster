const _ = require('lodash');
const React = require('react');
const R = React.DOM;

var MenuButton = React.createClass({
  propTypes: {
    onClick: React.PropTypes.func.isRequired,
    expanded: React.PropTypes.bool.isRequired
  },

  render: function() {
    var buttonCopy = '';
    if(this.props.expanded){
      buttonCopy = 'Close'
    } else {
      buttonCopy = 'Subscriptions'
    }

    return R.span({className: 'button-menu-wrapper', onClick: this.props.onClick},
      R.a({className: 'button button-menu'}, buttonCopy),
      R.a({className: 'button-arrow'}, '')
    )
  }
});

module.exports = MenuButton;