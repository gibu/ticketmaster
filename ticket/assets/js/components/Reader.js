const _ = require('lodash');
const React = require('react');
const R = React.DOM;
const ReaderHeader = React.createFactory(require('./ReaderHeader.js'));
const Feeds = React.createFactory(require('./Feeds.js'));

var Reader = React.createClass({
  propTypes: {
    feeds: React.PropTypes.array.isRequired,
    filters: React.PropTypes.array.isRequired
  },

  render: function() {
    return R.div({className: 'Reader'},
      ReaderHeader({filters: this.props.filters}),
      Feeds({feeds: this.props.feeds})
    );
  }
});

module.exports = Reader;