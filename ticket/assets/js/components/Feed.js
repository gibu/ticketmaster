var _ = require('lodash');
var React = require('react');
var R = React.DOM;
const RssAction = require('../actions/RssAction');

var Feed = React.createClass({
  propTypes: {
    feed: React.PropTypes.object.isRequired,
  },

  displayName: 'Feed',

  _onClickFeedDetail: function(e){
    e.preventDefault();
    RssAction.showDetail(this.props.feed.guid);
  },

  _onClickStar: function(e){
    e.preventDefault();
    RssAction.toggleFavourite(this.props.feed.guid);
  },

  render: function() {
    var feedDetail = {
      '__html': this.props.feed.description
    };
    var articleClass = '';
    var textComponent = [];
    if(this.props.feed.selected){
      articleClass = ' Selected';
      textComponent.push(R.p({className: 'Feed-Detail', dangerouslySetInnerHTML: feedDetail}))
    } else {
      textComponent.push(R.p({className: 'Feed-Short', onClick: this._onClickFeedDetail}, this.props.feed.short));
    }

    if(this.props.feed.favourite){
      articleClass += ' Favourite'
    }
    return R.article({className: 'Feed' + articleClass},
      R.span({className: 'Feed-Header'},
        R.span({className: 'Feed-Star', onClick: this._onClickStar},''),
        R.span({className: 'Feed-Title'}, this.props.feed.feed),
        R.span({className: 'Feed-Date'}, this.props.feed.prettyDate)
      ),
      R.h3({onClick: this._onClickFeedDetail}, this.props.feed.title),
      textComponent
    );
  }
});

module.exports = Feed;