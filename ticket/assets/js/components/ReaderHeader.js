var _ = require('lodash');
var React = require('react');
var R = React.DOM;
const RssAction = require('../actions/RssAction');
const QueryBox = React.createFactory(require('./queryBox.js'));
const AddFeed = React.createFactory(require('./AddFeed'));
const Sort = React.createFactory(require('./Sort'));

const ReaderHeaderItem = React.createClass({
  propTypes: {
    type: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    selected: React.PropTypes.bool
  },

  _onClick: function(e){
    e.preventDefault();
    RssAction.changeFilter(this.props.type);
  },

  render: function() {
    var className = '';
    if(this.props.selected){
      className = 'Selected';
    }

    return R.li({className: className, onClick: this._onClick},
      R.span({}, this.props.name)
    );
  }

});

const Item = React.createFactory(ReaderHeaderItem);

var ReaderHeader = React.createClass({
  propTypes: {

  },

  _onClick: function () {

  },

  render: function() {
    var headers = this.props.filters.map((filter) => {
        return Item(filter);
    });

    return R.div({className: 'ReaderHeader'},
      R.ul({className: 'Menu'},
        headers
      ),
      R.ul({},
        QueryBox(),
        AddFeed(),
        Sort()
      )
    );
  }
});

module.exports = ReaderHeader;