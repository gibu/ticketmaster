var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var _ = require('lodash');
var moment = require('moment');
var AppDispatcher = require('../dispatcher/Dispatcher.js');
const ActionTypes = require('../constants/RssConstants').ACTION_TYPES;
const Events = require('../constants/RssConstants').EVENTS;
const RssAction = require('../actions/RssAction');


var _rssFeeds = null;
var _flattenFeeds = null;
var _selectedFeed = null;
var _filters = [];
var _query = null;
var _sort = 'date_desc';
var _allFilters = [
  {
    type: 'unread',
    name: 'Unread'
  },
  {
    type: 'starred',
    name: 'Starred'
  },
  {
    type: 'all',
    name: 'All'
  }
];
var _fetchingFeedsInProgress = false;

var _fetchFeeds = function(){
  if(!_fetchingFeedsInProgress){
    _fetchingFeedsInProgress = true;
    RssAction.fetchFeeds();
  }
}


//var _rssFeeds = [];

const CHANGE_EVENT = Events.UPDATE;

var RssStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    console.log('Emit change');
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  getFeeds: function() {
    if(!_rssFeeds){
      _fetchFeeds();
      return [];
    }
    var query = {};
    var hasAllFilter = (_filters.indexOf('all') !== -1);
    var feeds = _flattenFeeds;
    var sortParams = _getSortParams();
    feeds = _.sortByOrder(feeds,...sortParams);
    if(_query) {
      feeds = _.filter(feeds, (feed) => {
        return (feed.key.indexOf(_query) !== -1);
      });
    }

    if(_selectedFeed){
      query.feed = _selectedFeed;
    }

    //apply filters
    if(_filters.indexOf('unread') !== -1 && !hasAllFilter){
      query.read = false;
    }


    if(_filters.indexOf('starred') !== -1 && !hasAllFilter){
      query.favourite = true;
    }

    if(Object.keys(query).length){
      return _.where(feeds, query);
    }
    return feeds;
  },

  getFeedsByIdentifier: function(identifier) {
    var feeds = _.find(_rssFeeds, {identifier});

    if(!feeds){
      return [];
    }
    return feeds.data;
  },

  getMenuItems: function() {
    if(!_rssFeeds){
      return [];
    }

    return _.map(_rssFeeds, (feed) =>{
      var item = {title: feed.title, selected: false};
      if(feed.title === _selectedFeed){
        item.selected = true;
      }
      return item;
    });
  },

  getFilters: function(){
    return _allFilters;
  }

});

//return array of array
var _getSortParams = function(){
  var filters = [['pubDateInSec'], ['desc']];
  switch(_sort){
    case 'date_asc' :
      filters = [['pubDateInSec'], ['asc']];
      break;
    case 'title_desc' :
      filters = [['title'], ['desc']];
      break;
    case 'title_asc' :
      filters = [['title'], ['asc']];
      break;
  }
  return filters;
};

var _setFeeds = function(feeds){
  _rssFeeds = feeds;
  _flatFeeds(_rssFeeds);
  _fetchingFeedsInProgress = false;
  RssStore.emitChange();
};

var _flatFeeds = function(feeds) {
  _flattenFeeds = [];
  feeds.forEach((feed) => {
    _flattenFeeds = _flattenFeeds.concat(feed.items);
  });
  _flattenFeeds = _flattenFeeds.map((feed) => {
    feed.read = false;
    feed.key = (feed.title + feed.pubDate + feed.prettyDate + feed.feed).toLowerCase();
    return feed;
  });
}

var _setSelectedFeed = function(feedName){
  _selectedFeed = feedName;
  var allPos = _filters.indexOf('all');
  RssStore.emitChange();
};

var _changeFilter = function(filter){
  var filterPos = _filters.indexOf(filter);
  if(filterPos !== -1){
    _filters.splice(filterPos, 1);
  } else{
    _filters.push(filter);
  }

  if(filter === 'all'){
    _selectedFeed = null;
    _filters = ['all'];
  } else {
    //remove 'all';
    let allPos = _filters.indexOf('all');
    if(allPos !== -1){
      _filters.splice(allPos, 1);
    }
  }

  _allFilters = _allFilters.map((filterOption) => {
    if(_filters.indexOf(filterOption.type) !== -1){
      filterOption.selected = true;
    }else {
      filterOption.selected = false;
    }
    return filterOption;
  });
  RssStore.emitChange();
};

var _setFeedToShow = function(guid){
  _flattenFeeds.forEach((feed) => {
    if(feed.guid === guid){
      feed.selected = true;
      feed.read = true;
    }else {
      feed.selected = false;
    }
  });
  RssStore.emitChange();
}

var _toggleFavourite = function(guid){
  _flattenFeeds.forEach((feed) => {
    if(feed.guid === guid){
      feed.favourite = !!!feed.favourite;
    }
  });
  RssStore.emitChange();
}

var _setQuery = function(query){
  _query = query.toLowerCase();
  RssStore.emitChange();
};

var _addFeed = function(feed){
  //check if feed exists by title
  var _currentFeedTitles = _.pluck(_rssFeeds, 'title');
  if(_currentFeedTitles.indexOf(feed.title) === -1){
    _rssFeeds.push(feed);
    _flatFeeds(_rssFeeds);
    RssStore.emitChange();
  };
};

var _changeSort = function(sort){
  _sort = sort;
  RssStore.emitChange();
}

RssStore.dispatchToken = AppDispatcher.register(function(payload){
  switch(payload.actionType){
    case ActionTypes.FETCH_FEEDS_DONE :
      _setFeeds(payload.feeds);
      break;
    case ActionTypes.SELECT_FEED :
      _setSelectedFeed(payload.feedName);
      break;
    case ActionTypes.CHANGE_FILTER :
      _changeFilter(payload.filter);
      break;
    case ActionTypes.SHOW_FEED_DETAIL :
      _setFeedToShow(payload.guid);
      break;
    case ActionTypes.TOGGLE_FAVOURITE :
      _toggleFavourite(payload.guid);
      break;
    case ActionTypes.CHANGE_QUERY :
      _setQuery(payload.query);
      break;
    case ActionTypes.ADD_FEED_DONE :
      _addFeed(payload.feed);
      break;
    case ActionTypes.CHANGE_SORT :
      _changeSort(payload.sort);
  }
});

module.exports = RssStore;