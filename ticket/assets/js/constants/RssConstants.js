module.exports = {
  EVENTS: {
    UPDATE: 'Update'
  },
  ACTION_TYPES: {
    FETCH_FEEDS_IN_PROGRES: 'Fetch Feeds in Progress',
    FETCH_FEEDS_ERROR: 'Fetch Feeds Error',
    FETCH_FEEDS_DONE: 'Fetch Feeds Done',
    SHOW_FEED_DETAIL: 'Show feed detail',
    SELECT_FEED: 'Select feed',
    APPLY_FILTERS: 'Apply filters',
    CHANGE_FILTER: 'Change Filter',
    TOGGLE_FAVOURITE: 'Toogle star',
    CHANGE_QUERY: 'Change query',
    ADD_FEED_IN_PROGRESS: 'Add Feed in Progress',
    ADD_FEED_ERROR: 'Add Feed Error',
    ADD_FEED_DONE: 'Add Feed Done',
    CHANGE_SORT: 'Change Sort'
  }
};