var request = require('request');
const ACTION_TYPES = require('../constants/RssConstants.js').ACTION_TYPES;
const Dispatcher = require('../dispatcher/Dispatcher.js');

class RssWeb{
  fetchFeeds(){
    var options = {
      url: window.location.origin + '/v1/feeds',
      json: true
    };

    request(options, function(error, response, body){
      console.warn('Error#fetchFeeds', error);
      if(error || response.statusCode !== 200){
        return Dispatcher.dispatch({
          actionType: ACTION_TYPES.FETCH_FEEDS_ERROR
        })
      }

      return Dispatcher.dispatch({
        actionType: ACTION_TYPES.FETCH_FEEDS_DONE,
        feeds: body
      });
    });
  }

  addFeed(url){
    var options = {
      url: window.location.origin + '/v1/feeds',
      method: 'POST',
      body: {url},
      json: true
    };

    console.log('Op---', options);
    request(options, function(error, response, body){
      console.warn('Error#fetchFeeds', error);
      if(error || response.statusCode !== 200){
        return Dispatcher.dispatch({
          actionType: ACTION_TYPES.ADD_FEED_ERROR
        })
      }

      return Dispatcher.dispatch({
        actionType: ACTION_TYPES.ADD_FEED_DONE,
        feed: body
      });
    });
  }
}

module.exports = new RssWeb();