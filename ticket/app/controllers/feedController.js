var async = require('async');
const Rss = require('../services/rss.js');

const FEEDS = [
  'http://feeds.mashable.com/mashable/startups',
  'http://www.tvn24.pl/najnowsze.xml'
];

class RssController{
  fetchFeeds (request, response){
    var endRequestCallback = function endFetching(error, result){
      if(error){
        return response.status(500).json({message: error.message})
      }
      return response.status(200).json(result);
    };

    var functionsToFetch = FEEDS.map((feedUrl) => {
        return function(callback){
          return Rss.fetch(feedUrl, callback);
        };
      });
    async.parallel(functionsToFetch, endRequestCallback);
  }

  createFeed (request, response){;
    var endRequestCallback = function endFetching(error, result) {
      if (error) {
        return response.status(500).json({message: error.message});
      }
      return response.status(200).json(result);
    };

    if(!request.body.url){
      return response.status(412).json({message: 'Missing required param url'});
    }

    Rss.fetch(request.body.url, endRequestCallback);
  }

};

module.exports =new RssController();