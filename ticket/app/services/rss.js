var request = require('request');
var xml2js = require('xml2js');
var parseString = xml2js.parseString;
var _ = require('lodash');
var moment = require('moment');
var htmlToText = require('html-to-text');

class Rss{
  fetch(url, callback) {
    var requestOption = {
      url,
      'Content-type': 'application/xml'
    };

    var parseXml2Json = function(body){
      parseString(body, (error, result) => {
        if(error){
          return callback(new Error('Problem with parseXML'));
        }
        var dResult = decorate(result.rss.channel[0]);
        return callback(null, dResult);
      });
    };

    var decorate = function(data) {
      var toReturn = {
        title: data.title[0],
        link: data.title[0],
        description: data.description[0],
        items: []
      };
      toReturn.items = _.map(data.item, (item) => {
          var pubDate = moment(item.pubDate[0]);
          return {
            title: item.title[0],
            feed: data.title[0],
            description: item.description[0],
            link: item.link[0],
            pubDate: item.pubDate[0],
            prettyDate: pubDate.format('D MMM YYYY'),
            pubDateInSec: pubDate.unix(),
            short: htmlToText.fromString(item.description[0], {ignoreHref: true, ignoreImage: true}).substring(0, 100) + '...',
            guid: (item.guid[0]._ || item.guid[0])
          };
        });
      return toReturn;
    };

    request(requestOption, function requestCallback(error, response, body){
      if(error){
        return callback(new Error('Problem with fetch data'));
      }
      if(response.statusCode !== 200){
        return callback(new Error('Data cannot be fetched'));
      }
      parseXml2Json(body);
    });

  }
}

module.exports = new Rss();