jest.dontMock('../rss.js');
jest.dontMock('moment');
jest.dontMock('lodash');
jest.dontMock('fs');

var rss = require('../rss.js');
var _ = require('lodash');
var request = require('request');
var fs = require('fs');
var xml2js = require('xml2js');
var htmlToText = require('html-to-text');
htmlToText.fromString.mockReturnValue('daadadadada');

var url = 'http://onet.pl';
var callback = null;
var feedMock = JSON.parse(fs.readFileSync('./app/services/__tests__/feed.txt', {encoding:  'utf8'}));

describe("Rss service", function() {
  beforeEach(function(){
    callback = jest.genMockFunction();
  });

  it("and fetch function should be defined", function() {
    expect(rss.fetch).toBeDefined();
  });

  describe("fetch", function(){
    describe("should request", function(){
      it("be called with", function(){
        rss.fetch(url, callback);
        expect(request).toBeCalledWith({url: url, 'Content-type': 'application/xml'}, jasmine.any(Function));
      });
    });

    describe("when request return error", function(){
      beforeEach(function(){
        request.mockImplementation(function(options, _callback){;
          return _callback({});
        });
      });

      it("callback should be called with error Problem with...", function(){
        rss.fetch(url, callback);
        expect(callback).toBeCalledWith(jasmine.objectContaining({
          message: 'Problem with fetch data'
        }));
      });
    });

    describe("when request return data with statusCode 500", function() {
      beforeEach(function(){
        request.mockImplementation(function (options, _callback) {
          return _callback(null, {statusCode: 500});
        });
      });
      it("should return error when status code is not 200", function () {
        rss.fetch(url, callback);
        expect(callback).toBeCalledWith(jasmine.objectContaining({
          message: 'Data cannot be fetched'
        }));
      });
    });

    describe("when fetching feed is ok", function(){
      beforeEach(function(){
        request.mockImplementation(function (options, _callback) {
          return _callback(null, {statusCode: 200}, '');
        });
      });

      describe("and is problem with parsing xml", function() {
        beforeEach(function(){
          xml2js.parseString.mockImplementation(function (options, _callback) {
            console.log('test');
            return _callback({});
          });
        });

        it("should callback have been called with error", function(){
          rss.fetch(url, callback);
          expect(callback).toBeCalledWith(jasmine.objectContaining({
            message: 'Problem with parseXML'
          }));
        });
      });

      describe("and parse xml is ok", function() {
        beforeEach(function(){
          xml2js.parseString.mockImplementation(function (options, _callback) {
            return _callback(null, feedMock);
          });
        });

        it("should callback have been called with decorated data", function(){
          rss.fetch(url, callback);
          expect(callback).toBeCalled(null, jasmine.any(Object));
        });
      });
    });

  });
});
